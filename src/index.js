import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ListComponent from './shared/ListComponent';
import TestTakersComponent from './components/testTakerComponent/TestTakersComponent';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<TestTakersComponent />, document.getElementById('root'));
registerServiceWorker();
