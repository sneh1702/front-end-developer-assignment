import React from 'react';

class ListComponent extends React.Component{
    constructor(props) {
        super(props);
        var me = this;

        me.headers = [];
    
        
    }

    getHeaders() {
        return this.props.tableHeadings.map(function(header, index){
            return <th  key={index + "-Header"} className="column-title">{header}</th>
        })
    }

    getColumns() {
        var me = this;
        return me.props.tableData.map(function(data){
            debugger;
            return <tr key={data.userId}>{me.getCellsForTable(data)}</tr>
        });
    }

    getCellsForTable(data) {
        this.headers = this.props.tableHeadings;
        return this.headers && this.headers.map(function(header, index) {
            header =  header.charAt(0).toLowerCase() + header.substring(1)
        
            return <td key = {index + "-cell"}className=" ">{data[header]}</td>
        });
    }


    render() {
        var me = this;
        console.log(me.props);
        return (
           <div>
               <table className="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr className="headings">{me.getHeaders()}
                        </tr>
                    </thead>
                    <tbody>
                    {this.getColumns()}
                    </tbody>
                </table>
           </div>
        );
    }
}

export default ListComponent;