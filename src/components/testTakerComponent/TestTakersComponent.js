import React from 'react';
import ListComponent from "../../shared/ListComponent";
import server from '../../utils/server';

class TestTakersComponent extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            tableHeaders : [],
            tableData : []
        };
    }

    componentDidMount() {
        var me = this;
        me.loadTestTakersListData();
    }

    loadTestTakersListData() {
        var me = this;

        server.getData("users").then(function (res) {
            me.updateStateForUserData(res.data);
        });
    }

    updateStateForUserData(testTakersArray) {
        var tableHeadersData = this.getDynamicTableHeaders(testTakersArray[0]);

        this.setState({
            tableHeaders: tableHeadersData,
            tableData: testTakersArray
        });
    }

    getDynamicTableHeaders(testTaker) {
        var tableHeadersArray = [];

        tableHeadersArray = Object.keys(testTaker).map(key => {
            // UpperCasing the First Letter for the headers.
            return key.charAt(0).toUpperCase() + key.substring(1);
        });

        return tableHeadersArray;
    }
    

    render() {
        var me = this;
        
        return (
           <div>
               <ListComponent tableHeadings = {me.state.tableHeaders} tableData = {me.state.tableData}/>
           </div>
        );
    }
}

export default TestTakersComponent;