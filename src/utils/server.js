

/***
 * Created as a POC. Need further implementation as required
 */

import axios from 'axios';

var server  =function axiosUtil() {
    var axiosObj = axios.create({
        baseURL:'https://hr.oat.taocloud.org/v1/',
        timeout: 5000
    });

    return {
        // requires the url for get
        getData: function (url) {
            return axiosObj.get(url);
        },
        // requires URL and the post data object
        postData: function(url, data) {
            return axiosObj.post(url, data);
        }
    }
};
export default server();
